let lines = document.querySelectorAll("#minha-linda-tabela > tbody")[1];
let things = Array.prototype.slice.call(lines.querySelectorAll("tr"));
let value = 0;
let total = 0;

for(let i = 0; i < things.length - 1; i++){
    value = Number((things[i].cells[3]).innerText);
    total = total + value;
    if(value > 0){
        things[i].classList.add("table-success")
    }
    else{
        things[i].classList.add("table-danger")
    }
}
const totalvalue = document.querySelectorAll("#tabela-total > th.text-dark")[1];
totalvalue.innerText = total.toFixed(2);

function ToggleDarkMode(obj){
    obj.classList.toggle("bg-light");
    obj.classList.toggle("text-dark");
    document.getElementById('meu-container').classList.toggle("bg-dark");
    document.getElementById('my-title').classList.toggle("text-dark");

    let x = document.querySelectorAll("#table-colunas > th.bg-light");
    for(let i = 0; i < x.length ;i++){
    x[i].classList.toggle("bg-dark");
    x[i].classList.toggle("text-light");
    x[i].classList.toggle("text-dark");
    }
}    